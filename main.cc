
#define cimg_use_ffmpeg
#include "CImg.h"
using namespace cimg_library;

#include <vector>
using namespace std;

int main(int argc, char** argv)
{
	if(argc < 2) {fprintf(stderr,"no spinid\n"); return -1;}

	fprintf(stderr,"Loading... ");
	CImg<short> I;
	char filename[2048];
	sprintf(filename,"%s/cap.mp4",argv[1]);
	I.load_ffmpeg(filename);
	fprintf(stderr,"Done.\n");
	I.resize_halfXY();
	I.crop(0,0,0,0,I.width()-1,I.height()-1,162,I.spectrum()-1);
	I.display();

	CImg<short> J;
	{
		CImgList<float> gradI = I.get_channel(0).get_gradient("xyz");
		CImg<float> hessI = ( gradI[0].sqr() + gradI[1].sqr() ).sqrt().cut(0,16);
		J = hessI.normalize(0,255).blur(1);
	}

	J.display("grad");

	/*
	while(1)
	{
		CImg<float> row = J.get_row(400).permute_axes("xzyc");
		CImg<int> p = row.get_select("row",0);
		CImg<float> S = row;
		cimg_forY(S,y)
		{
			int A = 150;
			int x = p[0] + A*cos(2*3.14*(y-p[1])/S.height());
			if(x < 0) x=0;
			if(x > S.width()-1) x=S.width()-1;
			S(x,y,0,0) = 255;
		}
		S.display();
	}
	*/


	CImg<short> D = J.get_fill(256);

	// Shortest Path algorithm, in place.
	vector<short> xs[256];
	vector<short> ys[256];
	vector<short> zs[256];
	
	// Eventually init to the whole boundary
	//D.load("D.cimg");

	//Varience


	// Shortest Path

	int b = 0;
	cimg_for_borderXY(D,x,y,1)
	{
		if(!b)
		{
			xs[0].push_back(x);
			ys[0].push_back(y);
			zs[0].push_back(0);
		}
		b=(b+1)%10;
	}

	for(short d=0; d < 256; d++)
	{
		while(!xs[d].empty())
		{
			int x,y,z;
			do {			
				x = xs[d].back();
				y = ys[d].back();
				z = zs[d].back();
				xs[d].pop_back();
				ys[d].pop_back();
				zs[d].pop_back();
			} while(!J.containsXYZC(x,y,z,0));

			int distance = max(d,J(x,y,z));
			if(distance < D(x,y,z))
			{
				D(x,y,z)=distance;
				
				xs[distance].push_back(x+1); ys[distance].push_back(y); zs[distance].push_back(z);
				xs[distance].push_back(x-1); ys[distance].push_back(y); zs[distance].push_back(z);
				xs[distance].push_back(x); ys[distance].push_back(y+1); zs[distance].push_back(z);
				xs[distance].push_back(x); ys[distance].push_back(y-1); zs[distance].push_back(z);
				xs[distance].push_back(x); ys[distance].push_back(y); zs[distance].push_back((z+1)%J.depth());
				xs[distance].push_back(x); ys[distance].push_back(y); zs[distance].push_back((z+J.depth()-1)%J.depth());
			}
		}
	}

	D.save("D.cimg");

	D.display("distances");

	CImg<float> sample = I;
	CImg<float> unfiltered = sample;
	cimg_forXYZ(sample,x,y,z)
	{
		unfiltered(x,y,z,0) = sample(x,y,z,0) * 255.0/230.0;
		unfiltered(x,y,z,1) = sample(x,y,z,1) * 255.0/200.0;
		unfiltered(x,y,z,2) = sample(x,y,z,2) * 255.0/170.0;
	}
	CImg<float> alpha = D.channel(0);
	alpha.normalize(0,1).cut(0,0.5).normalize(0,1);
	alpha.display("alpha");
	alpha.resize(-100,-100,-100,3);
	
	CImg<float> filtered = alpha.get_mul(sample) + (1.0-alpha).get_mul(unfiltered);
	unfiltered.cut(0,255);
	
	filtered.cut(0,255);

	sample.get_append(unfiltered).append(filtered).save("watershed.png").display();
	filtered.display();


	return 0;
	// !!!!!!!!!!
	/*
	priority_queue<FloodedPixel, vector<FloodedPixel>, FloodedPixel> flood;

	flood.push(FloodedPixel(0,0,0));
	int counter = 0;
	while(!flood.empty())
	{
		FloodedPixel next = flood.top();
		flood.pop();

		if(!mask.containsXYZC(next.x,next.y,0,0)) continue; // Fell off the map

		int pathcost = next.maxpath > J(next.x,next.y) ? next.maxpath : J(next.x,next.y);
		if(next.x == 0 || next.y == 0 || next.x == mask.width()-1 || next.y == mask.height()-1) //Edge pixels all get set to 0
			pathcost = 0;

		if(mask(next.x,next.y) < pathcost) continue; // Already found a cheaper way here
		
		mask(next.x,next.y) = pathcost;
		flood.push(FloodedPixel(next.x-1,next.y-1,pathcost));
		flood.push(FloodedPixel(next.x+1,next.y+1,pathcost));
		flood.push(FloodedPixel(next.x-1,next.y+1,pathcost));
		flood.push(FloodedPixel(next.x+1,next.y-1,pathcost));
		counter++;
		fprintf(stderr,"%d\n",counter);
		maskdisp.display(mask);
	}



	return 0;
	*/
	//!!!!!!!!!!!!!!
	/*
	CImg<int> newMask;
	J.quantize(10);

	float alt = 255;
	J.draw_fill(0,0,0,&alt,1.0,mask,10);
	while(1)
	{
		int mx,my,mz;
		alt=-1;
		CImg<float> medianMask = mask.get_blur_median(3);
		cimg_forXYZ(mask,x,y,z)
		{
			if( !mask(x,y,z) && medianMask(x,y,z) )
			{
				if(J(x,y,z) > alt)
				{
					alt = J(x,y,z);
					mx=x; my=y; mz=z;
				}
			}
		}
		if(alt==-1) break;
		J.draw_fill(mx,my,mz,&alt,1.0,newMask,5);
		mask = mask+newMask;
		J.display();
	}

	float temp = 255;
	while(temp > 0)
	{
		float bigpix = J.max();
		int x=0,y=0,z=0;
		J.contains(bigpix,x,y,z);
		temp = -bigpix;
		J.draw_fill(x,y,z,&temp,1.0,mask,5);
		temp = -temp;
	}
	float color = 255;
	J.draw_fill(0,0,0,&color,1.0,mask,5);
	J.display();
	mask.display();

	return 0;
	*/
	//!!!!!!!!!!!!

	/*
	J.channel(0).fill(0);

	CImg<float> dI = I.get_laplacian().abs();
	dI.display();

	for(int y = 1; y < I.height()/2-1; y++)
	{
		for(int x = 1; x < I.width()/2-1; x++)
		{
			J(x,y,0) = dI(x,y,0) > J(x,y-1) ? dI(x,y,0) : J(x,y-1) > J(x-1,y-1) ? J(x,y-1) : J(x-1,y-1);
		}
	}	
	J.display();

	return 0;
	// !!!!!!!!!!!

	CImg<int> bg = I.get_crop(0,0,50,50);

	double m = 0.0;
	double v = 0.0;
	int median = 0;
	for(int c = 0; c < 3; c++)
	{
		v = bg.get_shared_channel(c).variance_mean(1,m);
		median = I.get_shared_channel(c).median();
		if(m > median)
		{
			J.get_shared_channel(c) /= m-3*sqrt(v);
			J.get_shared_channel(c).cut(0,1);
		}
		else
		{
			J.get_shared_channel(c) -= m+1*sqrt(v);
			J.get_shared_channel(c).cut(0,255);
		}
	}

	J.normalize(0,255);
	I.append(J).display();
	
	return 0;
	*/
	
}